<?php

/**
 * Block helper function to render the block.
 * @return array
 */
function _twitter_search_twitter_tweet_list_render_block() {
  $build = array();

  $tweets = guzzle_oauth_search_twitter_get_tweets(variable_get('guzzle_oauth_search_twitter_tweets_type', 'search'), variable_get('guzzle_oauth_search_twitter_tweets_term', ''), variable_get('guzzle_oauth_search_twitter_tweets_limit', 10));

  if ($tweets) {
    $build['twitter_tweet_list'] = array(
      '#theme' => 'guzzle_oauth_search_twitter_list',
      '#tweets' => array(),
    );
    foreach ($tweets as $tweet) {
      $build['twitter_tweet_list']['#tweets'][] = array(
        '#theme' => 'guzzle_oauth_search_twitter_tweet',
        '#tweet' => $tweet,
      );
    }
  }

  return $build;
}