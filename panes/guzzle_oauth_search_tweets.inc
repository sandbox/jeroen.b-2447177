<?php
/**
 * This plugin array is more or less self documenting
 */
$plugin = array(
  'title' => t('Guzzle oAuth Search Tweets'),
  'description' => t('Guzzle oAuth Search Tweets'),
  'single' => TRUE,
  'category' => array('Guzzle oAuth Search'),
  'all contexts' => TRUE,
  'defaults' => array(
    'guzzle_oauth_search_tweets_type' => 'search',
    'guzzle_oauth_search_tweets_term' => '',
    'guzzle_oauth_search_tweets_limit' => 10,
  ),
  'admin info' => 'guzzle_oauth_search_tweets_admin_info',
  'edit form' => 'guzzle_oauth_search_tweets_edit_form',
  'render callback' => 'guzzle_oauth_search_tweets_render',
);

function guzzle_oauth_search_tweets_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = array();

  $tweets = guzzle_oauth_search_twitter_get_tweets($conf['guzzle_oauth_search_tweets_type'], $conf['guzzle_oauth_search_tweets_term'], $conf['guzzle_oauth_search_tweets_limit']);

  if ($tweets) {
    $block->content['tweet_list'] = array(
      '#theme' => 'guzzle_oauth_search_twitter_list',
      '#tweets' => array(),
    );
    foreach ($tweets as $tweet) {
      $block->content['tweet_list']['#tweets'][] = array(
        '#theme' => 'guzzle_oauth_search_twitter_tweet',
        '#tweet' => $tweet,
      );
    }
  }

  return $block;
}

function guzzle_oauth_search_tweets_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['guzzle_oauth_search_tweets_type'] = array(
    '#type' => 'select',
    '#title' => t('Search type'),
    '#options' => array(
      'search' => t('Search'),
      'user' => t('User'),
      'hashtag' => t('Hashtag'),
    ),
    '#default_value' => $conf['guzzle_oauth_search_tweets_type'],
  );

  $form['guzzle_oauth_search_tweets_term'] = array(
    '#type' => 'textfield',
    '#title' => 'Search term',
    '#default_value' => $conf['guzzle_oauth_search_tweets_term'],
  );

  $form['guzzle_oauth_search_tweets_limit'] = array(
    '#type' => 'textfield',
    '#title' => 'Tweet limit',
    '#default_value' => $conf['guzzle_oauth_search_tweets_limit'],
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function guzzle_oauth_search_tweets_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

function guzzle_oauth_search_tweets_admin_info($subtype, $conf) {
  $block = new stdClass();
  $block->title = 'Guzzle oAuth Search Tweets';
  $block->content = array(
    '#markup' => t('Twitter !type !term with a limit of !limit tweets.', array('!type' => $conf['guzzle_oauth_search_tweets_type'], '!term' => $conf['guzzle_oauth_search_tweets_term'], '!limit' => $conf['guzzle_oauth_search_tweets_limit']))
  );
  return $block;
}
