<div class="tweet">
  <span class="text"><?php print $tweet->text; ?></span>
  <span class="by">by <?php print l('@' . $tweet->user->screen_name, 'https://twitter.com/' . $tweet->user->screen_name); ?></span>
  <span class="at">at <?php print format_date(strtotime($tweet->created_at)); ?></span>
</div>